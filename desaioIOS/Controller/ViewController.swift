//
//  ViewController.swift
//  desaioIOS
//
//  Created by Ádria Cardoso on 02/07/2018.
//  Copyright © 2018 Ádria Cardoso. All rights reserved.
//

import UIKit
import Alamofire
import Toast_Swift

class ViewController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate {
    

    @IBOutlet weak var carrinhoBarItem: UIBarButtonItem!
    @IBOutlet weak var collectionView: UICollectionView!
    
    var products: [[String: Any]] = [[String: Any]]()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadData()
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "bg.png")!)
        self.collectionView?.backgroundColor = UIColor.clear
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func loadData(){
        let url = URL(string: "http://ec2-54-171-222-219.eu-west-1.compute.amazonaws.com:8484/lista/")
        Alamofire.request(url!).responseJSON { (response) in
            print(response.value)
            if let responseValue = response.result.value as! [String: Any]?{
                if let responseProducts = responseValue["products"] as! [[String: Any]]? {
                    print(responseProducts)
                    self.products = responseProducts
                    self.collectionView?.reloadData()
                }
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 6
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! CollectionViewCell
        if self.products.count > 0 {
            let eachProduct = self.products[indexPath.row]
            cell.nomeProduto?.text = (eachProduct["name"] as? String) ?? ""
            cell.precoProduto?.text = (eachProduct["priceMinimum"] as? String) ?? ""
//            cell.imagemProduto.sd_setImageWithURL((eachProduct["url"]) as? UIImage) ?? ""
        }
        return cell
    }
    
    @IBAction func botaoAdicionarAoCarrinho(_ sender: Any) {
        self.view.makeToast("Adicionado ao carrinho", duration: 3.0, position: .center)
        carrinhoBarItem.addBadge(number: 1)
    }
    
    
    @IBAction func menuBotao(_ sender: Any) {
        self.view.makeToast("Menu aberto", duration: 3.0, position: .center)
        
    }
}
