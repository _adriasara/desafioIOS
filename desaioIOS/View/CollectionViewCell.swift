//
//  CollectionViewCell.swift
//  desaioIOS
//
//  Created by Ádria Cardoso on 03/07/2018.
//  Copyright © 2018 Ádria Cardoso. All rights reserved.
//

import UIKit

class CollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var precoProduto: UILabel!
    @IBOutlet weak var nomeProduto: UILabel!
    @IBOutlet weak var adicionarCarrinhoBotao: UIButton!
    @IBOutlet weak var imagemProduto: UIImageView!
}
